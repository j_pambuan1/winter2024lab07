public class Card{
	//fields
	private String suits; // Hearts, Diamonds, Spades, Clubs
	private String value; // Ace, Two, Three... Ten, Jack, King, Queen
	
	//constructor
	public Card(String suits, String value){
		this.suits = suits;
		this.value = value;
	}
	
	//getters 
	public String getSuit(){
		return this.suits;
	}
	
	public String getValue(){
		return this.value;
	}
	
	public String toString(){
		return this.value + " of " + this.suits;
	}

	public double calculateScore(){
		int score = Integer.parseInt(this.value);
		if(this.suits.equals("Hearts")){
			return score + 0.4;
		}else if(this.suits.equals("Diamonds")){
			return score + 0.3;
		}else if(this.suits.equals("Clubs")){
			return score + 0.2;
		}else{
			return score + 0.1;
		}
	}
	
}